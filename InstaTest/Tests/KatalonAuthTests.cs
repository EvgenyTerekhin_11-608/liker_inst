﻿using System;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using InstaTest;
using InstaTest.Tests;
using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Firefox;
using OpenQA.Selenium.Support.UI;

namespace SeleniumTests
{
    [TestFixture]
    public class AuthTestCase : KatalonAuthTestBase
    {
        [Test]
        public void InValid_Auth_1()
        {
            AppManager.ContactHelper.BaseInfo.Login = AppManager.DataHelper.GetData("Settings", "FakeLogin1");
            AppManager.ContactHelper.BaseInfo.Password = AppManager.DataHelper.GetData("Settings", "FakePass1");
            Assert.Catch(AppManager.LoginHelper.Auth);
        }

        [Test]
        public void InValid_Auth_2()
        {
            AppManager.ContactHelper.BaseInfo.Login = AppManager.DataHelper.GetData("Settings", "FakeLogin2");
            AppManager.ContactHelper.BaseInfo.Password = AppManager.DataHelper.GetData("Settings", "FakePass2");
            Assert.Catch(AppManager.LoginHelper.Auth);
        }

        [Test]
        public void Valid_Auth()
        {
            AppManager.ContactHelper.BaseInfo.Login = AppManager.DataHelper.GetData("Settings", "Login");
            AppManager.ContactHelper.BaseInfo.Password = AppManager.DataHelper.GetData("Settings", "Password");
            AppManager.LoginHelper.Auth();
            Assert.True(AppManager.LoginHelper.IsLogged(AppManager.ContactHelper.BaseInfo.Login));
        }

        //FAKE DATA))))
        [TestCase("brager12", "123123")]
        public void Invalid_Auth_SuccessAuth(string username, string password)
        {
            Assert.False(AppManager.LoginHelper.IsLogged(username));
        }
    }
}