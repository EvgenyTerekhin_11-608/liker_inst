﻿using System;
using System.Text;
using System.Threading;
using InstaTest.Helpers;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;

namespace InstaTest
{
    public class ApplicationManager
    {
        #region property

        private IWebDriver driver;

        public IWebDriver Driver => driver;

        public NavigationHelper NavigationHelper => _navigationHelper;

        public LoginHelper LoginHelper => _loginHelper;

        public ActionHelper ActionHelper => _actionHelper;

        public ContactInfoHelper ContactHelper => _contactInfoHelper;
        
        public DataHelper DataHelper => _dataHelper;

        private StringBuilder verificationErrors;
        private string baseURL;
        private bool acceptNextAlert = true;
        private NavigationHelper _navigationHelper { get; set; }
        private LoginHelper _loginHelper { get; set; }
        private ActionHelper _actionHelper { get; set; }
        private ContactInfoHelper _contactInfoHelper { get; set; }
        private DataHelper _dataHelper { get; set; }

        private static ThreadLocal<ApplicationManager> app = new ThreadLocal<ApplicationManager>();

        #endregion

        #region constructor

        public ApplicationManager()
        {
            driver = new ChromeDriver();
            driver.Manage().Window.Maximize();
            baseURL = "http://localhost";
            verificationErrors = new StringBuilder();
            _loginHelper = new LoginHelper(this);
            _dataHelper = new DataHelper(this);
            _navigationHelper = new NavigationHelper(this, baseURL);
            _contactInfoHelper = new ContactInfoHelper(this);
            _actionHelper = new ActionHelper(this);
        }

        #endregion

        #region actions

        public void Stop() => driver.Quit();

        #endregion

        public static ApplicationManager GetInstance()
        {
            if (app.IsValueCreated) return app.Value;
            var newInstance = new ApplicationManager();
            newInstance.NavigationHelper.NavigateToAuthUrl();
            app.Value = newInstance;
            return app.Value;
        }

        ~ApplicationManager()
        {
            try
            {
                driver.Quit();
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
        }
    }
}