﻿using OpenQA.Selenium;

namespace InstaTest
{
    public interface IKatalonTest
    {
        void SetupTest();
        void TeardownTest();
        void TestCase();
        bool IsElementPresent(By by);
        bool IsAlertPresent();
        string CloseAlertAndGetItsText();
    }
}