﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Xml.Linq;

namespace InstaTest
{
    public static class XmlExtentions
    {
        public static string GetValueByTagsPath(this XDocument xdocument, params string[] path)
        {
            bool ReturnIfNull(object x) => x == null;

            var xelement = xdocument.Element(path[0]);
            if (ReturnIfNull(xelement)) return null;
            for (var i = 1; i<path.Length; i++)
            {
                if (ReturnIfNull(xelement)) return null;
                xelement = xelement    .Element(path[i]);
            }

            return xelement.Value;
        }
    }
}