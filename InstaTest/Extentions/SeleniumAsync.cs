﻿using System;
using System.Collections.ObjectModel;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;

namespace InstaTest
{
    public static class SeleniumAsync
    {
        public static T1 Await<T1>(this IWebDriver webdriver, Func<IWebDriver, T1> func) =>
            new WebDriverWait(webdriver, new TimeSpan(10 * 10000000)).Until(func);
        
        public static T1 FastAwait<T1>(this IWebDriver webdriver, Func<IWebDriver, T1> func) =>
            new WebDriverWait(webdriver, new TimeSpan(5 * 10000000)).Until(func);

        public static void GoToUrl(this IWebDriver webDriver, string url) => webDriver.Navigate().GoToUrl(url);

        public static void Maximase(this IWebDriver webDriver, string url) => webDriver.Manage().Window.Maximize();

        public static IWebElement GetElementByClassName(this IWebDriver webDriver, string name)
            => webDriver.FindElement(By.ClassName(name));

        public static ReadOnlyCollection<IWebElement> GetElementsByClassName(this IWebDriver webDriver, string name)
            => webDriver.FindElements(By.ClassName(name));
   
        public static ReadOnlyCollection<IWebElement> GetElementsByTagName(this IWebDriver webDriver, string name)
            => webDriver.FindElements(By.PartialLinkText(name));
    }
}