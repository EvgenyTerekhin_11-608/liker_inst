﻿namespace InstaTest.Helpers
{
    public class ContactInfoHelper : BaseHelper
    {
        private ContactInfoCreater _contactInfoCreater { get; set; }
        public BaseInfo BaseInfo { get; }

        public ContactInfoHelper(ApplicationManager manager) : base(manager)
        {
            BaseInfo = new ContactInfoCreater().Create();
        }
    }
}