﻿using OpenQA.Selenium;

namespace InstaTest.Helpers
{
    public class BaseHelper
    {

        private ApplicationManager manager { get; set; }
        protected IWebDriver _webDriver { get; set; }
        protected bool acceptNextAlert { get; set; }

        public BaseHelper(ApplicationManager manager)
        {
            this.manager = manager;
            _webDriver = manager.Driver;
            acceptNextAlert = true;
        }

        #region helpers

        private string CloseAlertAndGetItsText()
        {
            try
            {
                IAlert alert = _webDriver.SwitchTo().Alert();
                string alertText = alert.Text;
                if (acceptNextAlert)
                {
                    alert.Accept();
                }
                else
                {
                    alert.Dismiss();
                }

                return alertText;
            }
            finally
            {
                acceptNextAlert = true;
            }
        }

        #endregion
    }
    
}