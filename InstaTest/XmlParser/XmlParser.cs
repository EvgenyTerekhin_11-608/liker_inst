﻿using System.Collections;
using System.Collections.Generic;
using System.Dynamic;
using System.Linq;
using System.Xml;
using System.Xml.Linq;

namespace InstaTest.Helpers
{
    public class XmlParser
    {
        private XDocument _xDocument { get; set; }

        public XmlParser(string path)
        {
             _xDocument = XDocument.Load(path);
        }

        public string getData(params string[] path)
        {
           return _xDocument.GetValueByTagsPath(path);
        }

        public string getLogin => _xDocument.GetValueByTagsPath("Settings", "Login");
        public string getPassword => _xDocument.GetValueByTagsPath("Settings", "Password");
        public string getBaseUrl => _xDocument.GetValueByTagsPath("Settings", "BaseUrl");
    }
}